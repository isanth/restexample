//
//  NSObject+UPNParsing.m
//  FactoryApp
//
//  Created by Adrian Chojnacki on 31/07/15.
//  Copyright (c) 2015 Up Next. All rights reserved.
//

#import "NSObject+UPNParsing.h"

@implementation NSObject (UPNParsing)

- (id)objectOrNil
{
    if ([self isKindOfClass:[NSNull class]]) {
        return nil;
    } else {
        return self;
    }
}

@end
