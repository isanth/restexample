//
//  ContactTableViewCell.m
//  RestExample
//
//  Created by Kamil Michalik on 03/03/16.
//  Copyright © 2016 Kamil Michalik. All rights reserved.
//

#import "ContactTableViewCell.h"

@implementation ContactTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)configureWitchContact:(Contact *)cc
{
    self.idNum.text = cc.idNum;
    self.fname.text = cc.firstname;
    self.lname.text = cc.lastname;
    self.email.text = cc.email;
    self.phone.text = [[cc.phone allValues] componentsJoinedByString:@", "];
    self.addres.text = cc.addres;
    self.twitter.text = cc.twitter;
    self.createDate.text = cc.createDate;
}
@end
