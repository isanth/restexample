//
//  SecondVCViewController.h
//  RestExample
//
//  Created by Kamil Michalik on 03/03/16.
//  Copyright © 2016 Kamil Michalik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactTableViewCell.h"
#import "Contact.h"

@interface SecondVCViewController : UITableViewController
@property (strong, nonatomic) NSArray *contacts;
@property (strong, nonatomic) Contact *contact;

@end
