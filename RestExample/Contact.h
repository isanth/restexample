//
//  Contact.h
//  RestExample
//
//  Created by Kamil Michalik on 03/03/16.
//  Copyright © 2016 Kamil Michalik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject

@property (copy, nonatomic)  NSString *idNum;
@property (copy, nonatomic)  NSString *firstname;
@property (copy, nonatomic)  NSString *lastname;
@property (copy, nonatomic)  NSString *email;
@property (copy, nonatomic)  NSDictionary *phone;
@property (copy, nonatomic)  NSString *addres;
@property (copy, nonatomic)  NSString *twitter;
@property (copy, nonatomic)  NSString *createDate;

- (instancetype)initWithContactDict:(NSDictionary *)dict;

@end
