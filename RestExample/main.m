





//
//  main.m
//  RestExample
//
//  Created by Kamil Michalik on 03/03/16.
//  Copyright © 2016 Kamil Michalik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
