//
//  Contact.m
//  RestExample
//
//  Created by Kamil Michalik on 03/03/16.
//  Copyright © 2016 Kamil Michalik. All rights reserved.
//

#import "Contact.h"
#import "NSObject+UPNParsing.h"

@implementation Contact

- (instancetype)initWithContactDict:(NSDictionary *)dict
{
    if (self = [super init]) {
        
        _idNum = [dict[@"_id"] objectOrNil];
        _firstname = [dict[@"firstName"] objectOrNil];
        _lastname = [dict[@"lastName"] objectOrNil];
        _email = [dict[@"email"] objectOrNil];
        _phone = [dict[@"phone"] objectOrNil];
        _addres = [dict[@"address"] objectOrNil];
        _twitter = [dict[@"twitter"] objectOrNil];
        _createDate = [dict[@"createDate"] objectOrNil];
        
    }
    
    return self;
}
    
@end
