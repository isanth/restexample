//
//  NSObject+UPNParsing.h
//  FactoryApp
//
//  Created by Adrian Chojnacki on 31/07/15.
//  Copyright (c) 2015 Up Next. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (UPNParsing)

- (id)objectOrNil;

@end
