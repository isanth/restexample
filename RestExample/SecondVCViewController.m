//
//  SecondVCViewController.m
//  RestExample
//
//  Created by Kamil Michalik on 03/03/16.
//  Copyright © 2016 Kamil Michalik. All rights reserved.
//

#import "SecondVCViewController.h"


@interface SecondVCViewController ()
@property (strong, nonatomic) IBOutlet UILongPressGestureRecognizer *tapG;


@end

@implementation SecondVCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contacts = @[];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 300.0f;
    
    self.tapG = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(pressedLong:)];
    [self.view addGestureRecognizer:self.tapG];
    
    [self.tableView setContentInset:UIEdgeInsetsMake(24, 0, 0, 0)];

    
    // Fetch contacts
    [self fetchContacts:^(NSArray * arr) {
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            if (arr.count) {
                self.contacts = arr;
            }
            else {
                [self showNoData];
            }
            
            [self.tableView reloadData];
            
            });
        }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pressedLong:(id)sender {
   
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(Contact *)contactForIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *ccDictionary = self.contacts[indexPath.row];
    _contact = [[Contact alloc] initWithContactDict:ccDictionary];
    
    return _contact;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.contacts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Contact *cc = [self contactForIndexPath:indexPath];
    ContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactCell" forIndexPath:indexPath];
    
    [cell configureWitchContact:cc];
    
 

    return cell;
 }

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return 300.f;
}

#pragma mark - Private
-(void)fetchContacts:(void (^)(NSArray * arr))completionHandler
{
    // GET:
    
    NSURL *URL = [NSURL URLWithString:@"http://resftulexample.herokuapp.com/contacts"];
    // NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[ session dataTaskWithURL:URL
             completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                 
                 id dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

                 if (completionHandler) {
                     completionHandler(dictionary);
                 }
             }] resume];
    
}

-(void)showNoData
{
    UIView *eV = [[UIView alloc] initWithFrame:self.tableView.frame];
    eV.backgroundColor = [UIColor whiteColor];
    UILabel *tt = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 300)];
    tt.text = @"No entries from backend";
    tt.font = [UIFont systemFontOfSize:18];
    tt.textAlignment = NSTextAlignmentCenter;
    tt.center = self.view.center;
    [eV addSubview:tt];

    [self.view addSubview:eV];
}


@end
